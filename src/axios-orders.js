import Axios from "axios";

const instance = Axios.create({
  baseURL: "https://react-burger-6732a.firebaseio.com/"
});

export default instance;
